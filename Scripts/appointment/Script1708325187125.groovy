import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('reusable-test/block-login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.selectOptionByValue(findTestObject('ObjectRecord/Appointment/drop_facility'), 'Seoul CURA Healthcare Center', false)

WebUI.check(findTestObject('ObjectRecord/Appointment/chk_hospital_readmission'))

WebUI.click(findTestObject('ObjectRecord/Appointment/radio_Medicare_programs'))

WebUI.setText(findTestObject('ObjectRecord/Appointment/input__visit_date'), '16/02/2024')

WebUI.setText(findTestObject('ObjectRecord/Appointment/inpt_comment'), 'okeeee')

WebUI.click(findTestObject('ObjectRecord/Appointment/btn_Book_Appointment'))

WebUI.waitForElementVisible(findTestObject('ObjectRecord/Appointment Confirmation/h2_Appointment Confirmation'), 0)

WebUI.verifyElementText(findTestObject('ObjectRecord/Appointment Confirmation/p_Hongkong CURA Healthcare Center'), 'Seoul CURA Healthcare Center')

WebUI.verifyElementText(findTestObject('ObjectRecord/Appointment Confirmation/p_Yes'), 'Yes')

WebUI.verifyElementText(findTestObject('ObjectRecord/Appointment Confirmation/p_Medicaid'), 'Medicare')

WebUI.verifyElementText(findTestObject('ObjectRecord/Appointment Confirmation/p_21022024'), '16/02/2024')

WebUI.verifyElementText(findTestObject('ObjectRecord/Appointment Confirmation/p_axsx'), 'okeeee')

WebUI.click(findTestObject('ObjectRecord/Appointment Confirmation/a_Go to Homepage'))

WebUI.closeBrowser()

