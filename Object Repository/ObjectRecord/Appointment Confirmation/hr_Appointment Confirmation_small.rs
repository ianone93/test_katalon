<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>hr_Appointment Confirmation_small</name>
   <tag></tag>
   <elementGuidId>58fe27d9-f1e8-420d-a23e-a2d1a0e556b2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//section[@id='summary']/div/div/div/hr</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>hr.small</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>hr</value>
      <webElementGuid>d030affe-701e-478d-9eb4-f7e8df7c7db4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>small</value>
      <webElementGuid>14e1a209-227a-47d1-8c4f-56581b10e88c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;summary&quot;)/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 text-center&quot;]/hr[@class=&quot;small&quot;]</value>
      <webElementGuid>0502ee32-3eb8-4ec9-a3f6-ce4ec3511d2c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='summary']/div/div/div/hr</value>
      <webElementGuid>bb00d163-0d40-40e6-ba2e-c1ffad06d180</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//hr</value>
      <webElementGuid>389bfab3-5e61-46f9-b31a-bf2bc53ccc46</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
