<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS_Login DDT</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>10</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>03c78f0d-9903-4bdb-9e68-6207938fd744</testSuiteGuid>
   <testCaseLink>
      <guid>1fad605b-916b-495e-b225-4d29a7c25cf5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/login/login with variable</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>07cb3b6d-316a-4faa-a493-b0fad2883413</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Login/Login Data</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>07cb3b6d-316a-4faa-a493-b0fad2883413</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>c0e5e2cc-df86-471c-901b-090cda3c136f</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>07cb3b6d-316a-4faa-a493-b0fad2883413</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>6d38a439-eacf-4e22-8e7f-d1a01ef1b3db</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
